#!/usr/bin/env bash
set +e 

/usr/local/bin/docker-machine create --driver=xhyve \
    --xhyve-cpu-count=2 --xhyve-memory-size=3072 --xhyve-disk-size=16384 \
    --xhyve-virtio-9p=y mocker 

eval $(docker-machine env mocker) 
